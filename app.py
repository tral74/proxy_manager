# swagger = Swagger(app)
import logging

from api import app
from api.utils import utils_bp
from api.views import proxy_manager

app.register_blueprint(proxy_manager)
app.register_blueprint(utils_bp)


if __name__ == '__main__':
    app.run()
