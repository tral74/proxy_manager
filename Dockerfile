# syntax=docker/dockerfile:1
FROM python:3.7
WORKDIR /proxy_manager
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
COPY . .
RUN pip install -r requirements.txt
EXPOSE 5000
CMD gunicorn --worker-class gevent --workers 9 --bind 0.0.0.0:5000 app:app --max-requests 10000 --timeout 5 --keep-alive 5 --log-level info