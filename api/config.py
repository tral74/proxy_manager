class BaseConfig(object):
    MAX_ATTEMPTS = 500


class DevConfig(BaseConfig):
    REDIS_URL = "redis://localhost:6379/0"
    BAN_PERIOD_IN_SECONDS = 20


class ProdConfig(BaseConfig):
    REDIS_URL = "redis://redis:6379/0"
    BAN_PERIOD_IN_SECONDS = 21600  # 6 hours

