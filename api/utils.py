import random

from flask import Blueprint

from api import app, db

utils_bp = Blueprint('utils_bp', __name__)


# Custom Flask command
# Used to populate a database with fake data
@app.cli.command("gen-data")
def generate_data():
    def ip_gen():
        return (".".join(str(random.randint(1, 254)) for _ in range(4)) for _ in range(5000))

    if db.exists('uk') == 0:
        db.rpush('uk', *ip_gen())
        print('Data for UK is generated')

    if db.exists('us') == 0:
        db.rpush('us', *ip_gen())
        print('Data for US is generated')


@app.errorhandler(Exception)
def error_handler(error):
    app.logger.exception(error)
    return {}, 500
