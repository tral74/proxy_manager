from flask import request, Blueprint
from flask_marshmallow import Schema
from marshmallow import ValidationError, fields
from marshmallow.validate import OneOf

from api import db, app


class ReportErrorInputSchema(Schema):
    ip = fields.IPv4(required=True)
    country_code = fields.Str(required=True, validate=OneOf(['uk', 'us']))


report_error_input = ReportErrorInputSchema()

proxy_manager = Blueprint('proxy_manager', __name__)


def is_banned(country_code: str, ip: str) -> bool:
    """
    Check if IP address is banned
    Information about banned IPs is stored in Redis as a key in the format "{country_code} _ {ip}"
    If such key exists, then IP is blocked
    """
    key = f"{country_code}_{ip}"
    return db.exists(key) > 0


@proxy_manager.route('/get-proxy/<country_code>/')
def get_proxy(country_code):
    # IP addresses for each country are stored in Redis as a list with name corresponding to the country code (in lowercase)
    # If list with name <country_code> does not exist, then that country is not supported
    if db.exists(country_code) == 0:
        return {'status': 'fail', 'reason': 'country_not_supported'}, 400

    else:
        # Iterating through the list to the nearest available IP
        # In extreme cases, the percentage of banned IP addresses in the list may be too high (up to 100)
        # In this case, the search will take too long (and in the case of 100% ban, there will be infinite loop)
        # To avoid this, the number of iterations is limited
        for _ in range(0, app.config['MAX_ATTEMPTS']):
            # Pop the last IP address (from tail) and "rotate" the list (push element to head)
            ip = db.rpoplpush(country_code, country_code)

            # If IP is not banned, return IP and break iteration
            if not is_banned(country_code, ip):
                return {'status': 'successful', 'ip': ip}

        # If the maximum number of attempts has been reached
        return {'status': 'fail', 'reason': 'no_ip_available'}, 204


@proxy_manager.route('/report-error/', methods=['POST'])
def report_error():
    # Parse and validate payload
    try:
        params = report_error_input.load(request.json)

    except ValidationError as ex:
        return {'status': 'fail', 'reason': ex.messages}, 400

    key = f"{params['country_code']}_{str(params['ip'])}"

    # If IP is already banned
    if is_banned(params['country_code'], str(params['ip'])):
        return {'status': 'fail', 'reason': 'already_banned'}, 409

    # Mark IP as banned
    else:
        db.set(name=key, value='banned', ex=app.config['BAN_PERIOD_IN_SECONDS'])
        return {'status': 'successful'}
