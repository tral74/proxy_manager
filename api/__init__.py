import logging
import os

from flask import Flask
from redis import from_url

from api.config import ProdConfig, DevConfig

app = Flask(__name__)

# Configuring app
if os.environ.get('FLASK_ENV', 'production') == 'production':
    app.config.from_object(ProdConfig)

else:
    app.config.from_object(DevConfig)

# Redis connection
db = from_url(url=app.config['REDIS_URL'], decode_responses=True)

# Setup logger
formatter = logging.Formatter("%(asctime)s %(levelname)-8s %(message)s', 'datefmt': '%d-%m-%Y %H:%M:%S'')")
handler = logging.FileHandler(filename='proxy_manager.log')
handler.setLevel(logging.INFO)
handler.setFormatter(formatter)
app.logger.addHandler(handler)
