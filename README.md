## proxy_manager


    Data storage - Redis.
    IP addresses for each country are stored in Redis as a list with name corresponding to the country code (in lowercase)
    Information about banned IPs is stored in Redis as a key (with TTL) in the format "{country_code} _ {ip}"


### **Routes**

##### GET /get-proxy/<country_code>/
Parameters
    
    country_code 
        type: str
        Valid values: uk, us

Responses
    
    code: 200
    {
        "status": "successful", 
        "ip": "<ip>"
    }

    code: 204
    {
        "status": 'fail', 
        "reason": "no_ip_available"
    }

    code: 400
    {
        "status": 'fail', 
        "reason": "country_not_supported"
    }



##### POST /report-error/
Parameters (example)

    {   
        "ip": "1.2.3.4",
        "country_code": "uk"
    }

    Valid country codes: uk, us 

Responses

    code: 20
    {'status': 'successful'}

    code: 409
    {'status': 'fail', 'reason': 'already_banned'}

### To start the application, please run the command
    docker-compose up -d

API should be available by URL http://127.0.0.1:5000/ 
